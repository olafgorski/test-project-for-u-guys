from enum import IntEnum


class Months(IntEnum):
    JANUARY = 1
    FEBRUARY = 2
    MARCH = 3
    APRIL = 4
    MAY = 5
    JUNE = 6
    JULY = 7
    AUGUST = 8
    SEPTEMBER = 9
    OCTOBER = 10
    NOVEMBER = 11
    DECEMBER = 12

    @staticmethod
    def value_list():
        return [item.value for item in Months]


days_in_month_mapping = {
    Months.JANUARY.value: 31, Months.FEBRUARY.value: 28,
    Months.MARCH.value: 31, Months.APRIL.value: 30,
    Months.MAY.value: 31, Months.JUNE.value: 30,
    Months.JULY.value: 31, Months.AUGUST.value: 31,
    Months.SEPTEMBER.value: 30, Months.OCTOBER.value: 31,
    Months.NOVEMBER.value: 30, Months.DECEMBER.value: 31
}

SPECIAL_YEAR_CASES = {0: '', 1: 'rok', 2: '2 lata', 3: '3 lata', 4: '4 lata'}
SPECIAL_MONTH_CASES = {0: '', 1: 'miesiąc', 2: '2 miesiące', 3: '3 miesiące', 4: '4 miesiące'}
SPECIAL_DAY_CASES = {0: '', -1: 'wczoraj', -2: 'przedwczoraj', 1: 'jutro', 2: 'pojutrze'}
SPECIAL_DAY_CASES_TOGETHER_WITH_OTHER_UNITS = {0: '', 1: '1 dzień'}

FUTURE_MAPPING = {False: -1, True: 1}
