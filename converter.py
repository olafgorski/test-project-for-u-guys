import datetime

import constants
import utils


class TimeDifferenceCalculator:
    def __init__(self, future_date=None, past_date=None):
        self.months_diff = 0
        self.days_diff = 0
        self.years_diff = 0
        self.future_date = future_date
        self.past_date = past_date

    def calculate_month_difference(self):
        if not self.future_date or not self.past_date:
            raise ValueError("past or future date not set")
        while True:
            previous_month = utils.get_previous_month(self.future_date.month)
            days_in_previous_month = datetime.timedelta(constants.days_in_month_mapping.get(previous_month))
            if self.future_date - days_in_previous_month >= self.past_date:
                self.future_date -= days_in_previous_month
                self.months_diff += 1
            else:
                break
        return self.months_diff

    def set_years_and_months_from_months(self):
        self.years_diff, self.months_diff = self.months_diff // len(constants.Months), self.months_diff % 12
        return self.years_diff, self.months_diff

    def calculate_time_difference_and_set_dates(self, event_datetime, reference_datetime):
        self.future_date = max(event_datetime, reference_datetime).date()
        self.past_date = min(event_datetime, reference_datetime).date()
        self.calculate_time_difference()

    def calculate_time_difference(self):
        self.calculate_month_difference()
        self.set_years_and_months_from_months()
        self.calculate_day_difference()

    def calculate_day_difference(self):
        while self.future_date > self.past_date:
            self.days_diff += 1
            self.future_date -= datetime.timedelta(days=1)
        return self.days_diff


class DateTimeDescriptor(TimeDifferenceCalculator):

    def __init__(self):
        super().__init__()
        self.event_in_future = False

    def get_time_delta_description(self, event_datetime, reference_datetime=None):
        if not reference_datetime:
            reference_datetime = datetime.datetime.now()
        if reference_datetime < event_datetime:
            self.event_in_future = True
        self.calculate_time_difference_and_set_dates(event_datetime, reference_datetime)
        return self.convert_difference_to_text(event_datetime, reference_datetime)

    def convert_difference_to_text(self, event_datetime, reference_datetime):
        outcome = self.convert_date_to_text()
        outcome += self.get_hour(event_datetime, reference_datetime)
        return outcome

    def get_hour(self, event_datetime, reference_datetime):
        any_unit = any([self.days_diff, self.years_diff, self.months_diff])
        different_time = event_datetime.hour - reference_datetime.hour \
                         + event_datetime.minute - reference_datetime.minute
        if any_unit:
            return f' o {event_datetime.strftime("%H:%M")}'
        if different_time:
            return f'dziś o {event_datetime.strftime("%H:%M")}'
        return 'właśnie teraz'

    def convert_date_to_text(self):
        outcome = list()
        outcome.append(constants.SPECIAL_YEAR_CASES.get(self.years_diff, f"{self.years_diff} lat"))
        outcome.append(constants.SPECIAL_MONTH_CASES.get(self.months_diff, f"{self.months_diff} miesięcy"))
        if any([self.months_diff, self.years_diff]):
            outcome.append(constants.SPECIAL_DAY_CASES_TOGETHER_WITH_OTHER_UNITS.get(
                self.days_diff,
                f"{self.days_diff} dni")
            )
        else:
            outcome.append(constants.SPECIAL_DAY_CASES.get(
                self.days_diff * constants.FUTURE_MAPPING[self.event_in_future],
                f"{self.days_diff} dni")
            )
        return ' i '.join([value for value in outcome if value])
