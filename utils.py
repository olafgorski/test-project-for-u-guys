import constants


def is_leap_year(year):
    return year % 4 == 0


def get_previous_month(month):
    if month not in constants.Months.value_list():
        raise ValueError("Month has to be a valid month.")
    previous_month = month - 1
    if previous_month == 0:
        return constants.Months.DECEMBER.value
    return previous_month


def days_in_month(year, month):
    if is_leap_year(year) and month == constants.Months.FEBRUARY.value:
        return 29
    days = constants.days_in_month_mapping.get(month)
    if not days:
        return
    return days