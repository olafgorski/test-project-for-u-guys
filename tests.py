import datetime
import unittest

from converter import TimeDifferenceCalculator, DateTimeDescriptor


class TestThisAnnoyingThingForRecruitment(unittest.TestCase):

    def test_calculating_month_difference_no_difference(self):
        event_date = datetime.datetime.now()
        reference_date = datetime.datetime.now()
        am_so_tired = TimeDifferenceCalculator()
        am_so_tired.calculate_time_difference_and_set_dates(event_date, reference_date)
        self.assertEqual(am_so_tired.months_diff, 0)
        self.assertEqual(am_so_tired.years_diff, 0)
        self.assertEqual(am_so_tired.days_diff, 0)

    def test_calculating_month_difference_no_dates(self):
        am_so_tired = TimeDifferenceCalculator()
        self.assertRaises(ValueError, am_so_tired.calculate_month_difference)
        self.assertEqual(am_so_tired.months_diff, 0)
        self.assertEqual(am_so_tired.days_diff, 0)
        self.assertEqual(am_so_tired.years_diff, 0)

    def test_calculating_month_difference_1_month(self):
        event_date = datetime.datetime(2019, 1, 1)
        reference_date = datetime.datetime(2018, 12, 1)
        am_so_tired = TimeDifferenceCalculator()
        am_so_tired.calculate_time_difference_and_set_dates(event_date, reference_date)
        self.assertEqual(am_so_tired.months_diff, 1)
        self.assertEqual(am_so_tired.years_diff, 0)
        self.assertEqual(am_so_tired.days_diff, 0)

    def test_calculating_month_difference_less_than_a_month(self):
        event_date = datetime.datetime(2018, 1, 5)
        reference_date = datetime.datetime(2018, 1, 1)
        am_so_tired = TimeDifferenceCalculator()
        am_so_tired.calculate_time_difference_and_set_dates(event_date, reference_date)
        self.assertEqual(am_so_tired.months_diff, 0)
        self.assertEqual(am_so_tired.years_diff, 0)
        self.assertEqual(am_so_tired.days_diff, 4)

    def test_calculating_month_difference_more_than_a_year(self):
        event_date = datetime.datetime(2019, 2, 5)
        reference_date = datetime.datetime(2018, 1, 1)
        am_so_tired = TimeDifferenceCalculator()
        am_so_tired.calculate_time_difference_and_set_dates(event_date, reference_date)
        self.assertEqual(am_so_tired.months_diff, 1)
        self.assertEqual(am_so_tired.years_diff, 1)
        self.assertEqual(am_so_tired.days_diff, 4)


class UsuallyDuringRecruitmentProcessIdExpectABitMoreOfAHeadsUpSoThatIcanArrangeMyTimeProperly(unittest.TestCase):

    def test_convert_the_same_date(self):
        event_date = datetime.datetime.now()
        descriptor = DateTimeDescriptor()
        outcome = descriptor.get_time_delta_description(event_date)
        self.assertEqual(outcome, 'właśnie teraz')

    def test_convert_tomorrow(self):
        event_date = datetime.datetime(2019, 1, 14, 14, 0)
        reference_date = event_date - datetime.timedelta(days=1)
        descriptor = DateTimeDescriptor()
        outcome = descriptor.get_time_delta_description(event_date, reference_date)
        self.assertEqual(outcome, 'jutro o 14:00')

    def test_convert_yesterday(self):
        event_date = datetime.datetime(2019, 1, 14, 14, 0)
        reference_date = event_date + datetime.timedelta(days=1)
        descriptor = DateTimeDescriptor()
        outcome = descriptor.get_time_delta_description(event_date, reference_date)
        self.assertEqual(outcome, 'wczoraj o 14:00')

    def test_convert_the_dey_before_yesterday(self):
        event_date = datetime.datetime(2019, 1, 14, 14, 0)
        reference_date = event_date + datetime.timedelta(days=2)
        descriptor = DateTimeDescriptor()
        outcome = descriptor.get_time_delta_description(event_date, reference_date)
        self.assertEqual(outcome, 'przedwczoraj o 14:00')

    def test_convert_a_month(self):
        event_date = datetime.datetime(2019, 1, 14, 14, 0)
        reference_date = datetime.datetime(2018, 12, 14, 14, 0)
        descriptor = DateTimeDescriptor()
        outcome = descriptor.get_time_delta_description(event_date, reference_date)
        self.assertEqual(outcome, 'miesiąc o 14:00')

    def test_convert_two_months(self):
        event_date = datetime.datetime(2019, 1, 14, 14, 0)
        reference_date = datetime.datetime(2018, 11, 14, 14, 0)
        descriptor = DateTimeDescriptor()
        outcome = descriptor.get_time_delta_description(event_date, reference_date)
        self.assertEqual(outcome, '2 miesiące o 14:00')

    def test_convert_a_year(self):
        event_date = datetime.datetime(2019, 1, 14, 14, 0)
        reference_date = datetime.datetime(2018, 1, 14, 14, 0)
        descriptor = DateTimeDescriptor()
        outcome = descriptor.get_time_delta_description(event_date, reference_date)
        self.assertEqual(outcome, 'rok o 14:00')

    def test_convert_two_years(self):
        event_date = datetime.datetime(2019, 1, 14, 14, 0)
        reference_date = datetime.datetime(2017, 1, 14, 14, 0)
        descriptor = DateTimeDescriptor()
        outcome = descriptor.get_time_delta_description(event_date, reference_date)
        self.assertEqual(outcome, '2 lata o 14:00')

    def test_convert_five_years_leap_year(self):
        event_date = datetime.datetime(2019, 1, 14, 14, 0)
        reference_date = datetime.datetime(2014, 1, 14, 14, 0)
        descriptor = DateTimeDescriptor()
        outcome = descriptor.get_time_delta_description(event_date, reference_date)
        self.assertEqual(outcome, '5 lat i 1 dzień o 14:00')

    def test_convert_same_day_diff_hour(self):
        event_date = datetime.datetime(2019, 1, 14, 14, 0)
        reference_date = datetime.datetime(2019, 1, 14, 5, 0)
        descriptor = DateTimeDescriptor()
        outcome = descriptor.get_time_delta_description(event_date, reference_date)
        self.assertEqual(outcome, 'dziś o 14:00')
